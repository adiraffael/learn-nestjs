import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OurFile } from './ourfile.entity';
import { OurFileDTO } from './ourfile.dto';

@Injectable()
export class OurfileService {
    constructor(
        @InjectRepository(OurFile)
        private ourFileRepository : Repository<OurFile>
    ){}

    async showAll(){
        return await this.ourFileRepository.find();
    }

    async createNew(data: OurFileDTO){
        const ourFileNew = await this.ourFileRepository.create(data);
        await this.ourFileRepository.save(ourFileNew);
        return { message: 'success creating data', data: ourFileNew };
    }

    async getDetail(id: string){
        return await this.ourFileRepository.findOne({ where : {id} });
    }

    async updateData(id: string, data: Partial<OurFileDTO>){
        await this.ourFileRepository.update({id}, data);
        const record = await this.ourFileRepository.findOne({ where : {id} });
        return { message: 'success updating data', data: record }
    }

    async deleteOne(id: string){
        await this.ourFileRepository.delete({id});
        return { message: 'success deleting data' };
    }
}
