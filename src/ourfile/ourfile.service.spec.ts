import { Test, TestingModule } from '@nestjs/testing';
import { OurfileService } from './ourfile.service';

describe('OurfileService', () => {
  let service: OurfileService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OurfileService],
    }).compile();

    service = module.get<OurfileService>(OurfileService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
