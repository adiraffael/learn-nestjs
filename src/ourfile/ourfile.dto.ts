export class OurFileDTO{
    name: string
    description: string
    isPublic: boolean
}