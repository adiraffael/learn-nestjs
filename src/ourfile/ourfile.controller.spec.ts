import { Test, TestingModule } from '@nestjs/testing';
import { OurfileController } from './ourfile.controller';

describe('OurfileController', () => {
  let controller: OurfileController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OurfileController],
    }).compile();

    controller = module.get<OurfileController>(OurfileController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
