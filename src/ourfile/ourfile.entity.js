import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class OurFile{
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length:500 })
    name : string

    @Column('text')
    description : string

    @Column()
    isPublic : boolean

}