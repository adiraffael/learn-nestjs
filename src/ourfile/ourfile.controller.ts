import { Body, Controller, Get, Param, Post, Put, Delete, Render } from '@nestjs/common';
import { OurfileService } from './ourfile.service';
import { OurFileDTO } from './ourfile.dto';

@Controller('ourfile')
export class OurfileController {
    constructor(private OurfileService: OurfileService){}

    @Get('jsondata')
    async seeOutput(){
        return {data: await this.OurfileService.showAll()};
    }

    @Get()
    @Render('ourfile/index')
    root() {
    return { title: 'ourfile', message: 'Hello world!' };
  }

    @Post()
    createNew(@Body() data: OurFileDTO){
        return this.OurfileService.createNew(data);
    }

    @Get(':id')
    seeDetail(@Param('id') id: string){
        return this.OurfileService.getDetail(id);
    }

    @Put(':id')
    updateData(@Param('id') id: string, @Body() data: Partial<OurFileDTO>){
        return this.OurfileService.updateData(id, data);
    }

    @Delete(':id')
    deleteOne(@Param('id') id: string){
        return this.OurfileService.deleteOne(id);
    }
}
