import { Module } from '@nestjs/common';
import { OurFile } from './ourfile.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OurfileController } from './ourfile.controller';
import { OurfileService } from './ourfile.service';

@Module({
    imports : [ TypeOrmModule.forFeature([OurFile]) ],
    controllers: [OurfileController],
    providers: [OurfileService]
})
export class OurfileModule {}
