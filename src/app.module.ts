import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MongooseModule } from '@nestjs/mongoose'
import { OurfileModule } from './ourfile/ourfile.module';
import 'dotenv/config';
import { APP_FILTER } from '@nestjs/core';
import { HttpErrorFilter } from './shared/http.error.filter';
import { UserModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URI),
    OurfileModule,
    UserModule
  ],
  controllers: [AppController],
  providers: [
    AppService, {
      provide: APP_FILTER,
      useClass: HttpErrorFilter
    },
  ],
})
export class AppModule {}
