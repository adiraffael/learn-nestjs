import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { User, userSchema } from "./schemas/user.schema";
import { UsersController } from "./users.controller";
import { UserRepository } from "./users.repository";
import { UsersService } from "./users.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: userSchema }])
    ],
    controllers: [UsersController],
    providers: [UsersService, UserRepository]
})

export class UserModule {}